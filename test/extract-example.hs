-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2021 Simon Hengel <sol@typeful.net>
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>
-- SPDX-PackageSourceInfo: https://github.com/sol/markdown-unlit/blob/6fd1be584c3c78ed06d1b214cde39ce0ace31ccb/src/Text/Markdown/Unlit.hs

module Main where

import System.Environment
import System.IO
import System.Exit
import Text.Regex.Applicative


{-|
GHC calls @extract-example@ like so:

> extract-example label Foo.lhs /tmp/somefile [args]

[args] are custom arguments provided with -optF

The label is meant to be used in line pragmas, like so:

> {-# LINE 1 "label" #-}
-}
run :: [String] -> IO ()
run [fileName, infile, outfile, after] =
	process fileName infile outfile (Just after)
run [fileName, infile, outfile] =
	process fileName infile outfile Nothing
run _ = do
	name <- getProgName
	hPutStrLn stderr ("usage: " ++ name ++ " label infile outfile [after]")
	exitFailure


process :: FilePath -> FilePath -> FilePath -> Maybe String -> IO ()
process fileName infile outfile mafter = do
	contents <- readFileUtf8 infile
	case extractExample fileName mafter contents of
		Just codeblock ->
			writeFileUtf8 outfile codeblock
		Nothing -> do
			name <- getProgName
			hPutStrLn stderr (name ++ ": Unable to find main example in " ++ fileName ++ (maybe "" (\after -> " after “" ++ after ++ "”") mafter) ++ ".")
			exitFailure
	where
		readFileUtf8 name = openFile name ReadMode >>= \h -> hSetEncoding h utf8 >> hGetContents h
		writeFileUtf8 name str = withFile name WriteMode (\h -> hSetEncoding h utf8 >> hPutStr h str)


sigil :: String
sigil = "\n@\n"


examplePattern :: RE Char String
examplePattern = string sigil *> few anySym <* string sigil

-- | Count lines, assumes string starts with @\n@.
countLines :: String -> Int
countLines contents = length (filter (== '\n') contents)


{-|
Extracts the first large example in the module.
If @mafter@ argument is not 'Nothing', it will extract the first example that occurs after the string.

>>> extractExample "Main.hs" Nothing "\n@\ntest\n@\n"
Just "#line 3 \"Main.hs\"\ntest"

>>> extractExample "Main.hs" Nothing "foo\nbar\nbaz\n\n@\ntest\n@\n"
Just "#line 6 \"Main.hs\"\ntest"

>>> extractExample "Main.hs" Nothing "Section 1\nbar\nbaz\n\n@\ntest\n@\nSection 2\nbar\nbaz\n\n@\ntest2\n@\n"
Just "#line 6 \"Main.hs\"\ntest"

>>> extractExample "Main.hs" (Just "Section 1") "Section 1\nbar\nbaz\n\n@\ntest\n@\nSection 2\nbar\nbaz\n\n@\ntest2\n@\n"
Just "#line 6 \"Main.hs\"\ntest"

>>> extractExample "Main.hs" (Just "Section 2") "Section 1\nbar\nbaz\n\n@\ntest\n@\nSection 2\nbar\nbaz\n\n@\ntest2\n@\n"
Just "#line 13 \"Main.hs\"\ntest2"
-}
extractExample :: FilePath -> Maybe String -> String -> Maybe String
extractExample fileName mafter contents = do
	(contentsAfter, beforeLines) <- findAfter mafter ('\n' : contents)
	(prefix, codeblock, _suffix) <- findFirstInfix examplePattern contentsAfter
	let firstLineIndex = beforeLines + countLines (prefix ++ sigil)
	let prologue = "#line " ++ show firstLineIndex ++ " " ++ show fileName ++ ""
	return (prologue ++ "\n" ++ codeblock)


{-|
If @mafter@ is not 'Nothing', extracts text following given search term with the number of lines that was omitted.
If @mafter@ is 'Nothing', returns text as is.
-}
findAfter :: Maybe String -> String -> Maybe (String, Int)
findAfter (Just after) contents = do
	(prefix, afterMatch, contentsAfter) <- findFirstInfix (string after) contents
	return (afterMatch ++ contentsAfter, countLines (prefix ++ afterMatch))
findAfter Nothing contents = return (contents, 0)


main :: IO ()
main = getArgs >>= run
