# Hroch

Hroch is an opinionated static site generator based on [Hakyll](https://github.com/jaspervdj/hakyll). Out of the box it has the following features set up:

- [CommonMark](https://commonmark.org/) support through [pandoc](https://pandoc.org/).
- Clean URLs with links automatically adjusted.
- Support for galleries and generating thumbnails.
- Per-directory menus.
- Per-directory metadata.

Many of those features are also available as a `hroch` library, see the [API docs](https://tojnar.cz.gitlab.io/hroch/).

## Setting up

A hroch site is structured as follows:

- `content/` – actual content of the site (articles, news posts, photos and other files for downloading).
- `static/` – files used as a part of the layout (styles, scripts, images aside from content…), mostly kept as they are.
- `templates/` – building blocks for generating the HTML.

There is an example website in the `example/` directory.

## Usage

The `hroch` program needs to be run in the website’s repository. It supports the following subcommands:

- `hroch build` – produces a `public/` directory with the website to be viewed in a web browser or uploaded to the server. If source content changes, only the pages corresponding to changed files will be updated.
- `hroch rebuild` – cleans the `public/` directory and then runs the `build` subcommand.
- `hroch watch` – starts a web server that will serve the built website and automatically rebuild pages corresponding to changed files. While it is running, you can browse the site on <http://localhost:8000>.
- `hroch check` – checks for broken links in the generated website.

## Why was this created?

Hakyll’s flexibility is great but once you manage more than a few sites, just bumping dependencies takes a significant amount of time. And coming to a project after a while only to find that you need to compile it from scratch gets old.

## License

The source code is licensed under the terms of [MIT license](LICENSE).
