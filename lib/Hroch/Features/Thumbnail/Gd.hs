-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

module Hroch.Features.Thumbnail.Gd (
	thumbnailerGd,
) where

import Control.Monad (unless)
import Data.Maybe (fromMaybe)
import Graphics.GD (Image, imageSize, loadGifFile, loadJpegFile, loadPngFile, resizeImage, rotateImage, saveGifFile, saveJpegFile, savePngFile)
import Graphics.HsExif (ImageOrientation (..), RotationDirection (..), getOrientation, parseFileExif)
import Hroch.Features.Thumbnail (ThumbnailGenerator, ThumbnailStyle, ThumbSize, sizeDims)
import Hroch.Internal.Utils (both, lowercase)
import System.Directory (createDirectoryIfMissing)
import System.FilePath (takeDirectory, takeExtension)

-- | Resize an image to each of specified dimensions and save those to new files.
thumbnailerGd :: ThumbnailGenerator
thumbnailerGd style source destinations =
	unless (null destinations) $ do
		orientation <- findOrientation source
		mapM_ (\(size, destination) -> generateThumbnail style size orientation source destination) destinations

-- | Calculate dimensions of an image placed into a frame while maintaining aspect ratio.
calculateBoundedDims :: (Int, Int) -> (Int, Int) -> (Int, Int)
calculateBoundedDims size dims = both round $ calculateBoundedDims' (both fromIntegral size) (both fromIntegral dims)
	where
		calculateBoundedDims' :: (Double, Double) -> (Double, Double) -> (Double, Double)
		calculateBoundedDims' (bw, bh) (w, h)
			| w <= bw && h <= bh = (w, h)
			| w / h > bw / bh = (bw, h / w * bw)
			| otherwise = (w / h * bh, bh)

generateThumbnail :: ThumbnailStyle -> ThumbSize -> ImageOrientation -> FilePath -> FilePath -> IO ()
generateThumbnail style size orientation source destination =
	do
		img <- loadImage source
		orientedImg <- reorientImage orientation img
		dims <- imageSize orientedImg
		let newDims = calculateBoundedDims (sizeDims style size) dims
		newImg <- uncurry resizeImage newDims orientedImg
		createDirectoryIfMissing True (takeDirectory destination)
		putStrLn $ "  generated thumbnail " ++ destination
		saveImage destination newImg

-- | Load an image file for use with GD based on its extension.
loadImage :: FilePath -> IO Image
loadImage path =
	case lowercase $ takeExtension path of
		".gif" -> loadGifFile path
		".jpg" -> loadJpegFile path
		".jpeg" -> loadJpegFile path
		".png" -> loadPngFile path
		_ -> fail "Unknown image extension"

-- | Save an image file for use with GD based on its extension.
saveImage :: FilePath -> Image -> IO ()
saveImage path img =
	case lowercase $ takeExtension path of
		".gif" -> saveGifFile path img
		".jpg" -> saveJpegFile 90 path img
		".jpeg" -> saveJpegFile 90 path img
		".png" -> savePngFile path img
		_ -> fail "Unknown image extension"

-- | Try to determine orientation of a file.
findOrientation :: FilePath -> IO ImageOrientation
findOrientation file =
	do
		orientation <- parseFileExif file
		return $ case orientation of
			Left _ -> Normal
			Right exif -> fromMaybe Normal (getOrientation exif)

-- | Rotate image according to provided EXIF data.
reorientImage :: ImageOrientation -> Image -> IO Image
reorientImage Normal img = return img
reorientImage Mirror _ = fail "Mirrored images not supported"
reorientImage (Rotation rot) img = rotateImage (rotationToInt rot) img
reorientImage (MirrorRotation _) _ = fail "Mirrored images not supported"

-- | Convert a rotation stored in EXIF into a value accepted by GD.
rotationToInt :: RotationDirection -> Int
rotationToInt MinusNinety = 3
rotationToInt HundredAndEighty = 2
rotationToInt Ninety = 1
