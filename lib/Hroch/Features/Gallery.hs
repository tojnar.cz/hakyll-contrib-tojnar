-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

{-# LANGUAGE OverloadedStrings #-}

{-|
= Gallery generator

This is a [pandoc filter](https://pandoc.org/filters.html) that replaces a series of images inside a 'Div' with a class @figuregroup@ by a new series where each image is in a separate paragraph. This is to make pandoc’s [implicit_figures extension](https://pandoc.org/MANUAL.html#extension-implicit_figures) turn each of them into its own /figure/.

== Usage

@
{-# LANGUAGE OverloadedStrings #-}
import Hakyll
import Hroch.Features.Gallery (figureGroups)
import Hroch.Internal.Utils (runHakyllWithHelpFlagForTests)

markdownCompiler :: Compiler (Item String)
markdownCompiler = pandocCompilerWithTransform readOpts writeOpts filters
	where
		readOpts = defaultHakyllReaderOptions
		writeOpts = defaultHakyllWriterOptions
		filters = figureGroups

main :: IO ()
main = runHakyllWithHelpFlagForTests $ hakyll $ do
	match "**.md" $ do
		compile markdownCompiler
@

== Current limitations

* It will crash when a /figure group/ contains something other than images.
-}
module Hroch.Features.Gallery (
	figureGroups,
) where

import Text.Pandoc (
	Block (Div, Para),
	Inline (Image),
	Pandoc)
import Text.Pandoc.Walk (walk)

figureGroups :: Pandoc -> Pandoc
figureGroups = walk replaceFigureGroup

replaceFigureGroup :: Block -> Block
replaceFigureGroup (Div ("", ["figuregroup"], []) gallery) = Div ("", ["figuregroup"], []) (extractImages gallery)
replaceFigureGroup o = o

-- | If there are multiple images in one paragraph, move each image into its own paragraph.
extractImages :: [Block] -> [Block]
extractImages (Para (Image imgAttr content imgTarget : inlines) : blocks) = Para [Image imgAttr content imgTarget] : extractImages (Para inlines : blocks)
extractImages (Para (_ : inlines) : blocks) = extractImages (Para inlines : blocks)
extractImages (Para [] : blocks) = extractImages blocks
extractImages [] = []
