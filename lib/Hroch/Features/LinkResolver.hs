-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

{-# LANGUAGE ViewPatterns #-}

{-|
= Link target resolver

This is a [pandoc filter](https://pandoc.org/filters.html) that makes it possible to use links pointing to unprocessed files. It will modify the link targets, transparently resolving them to appropriate URLs.

By default, the filter will change the link targets using relative paths to a route from a Hakyll rule matching given path.

Optionally, you can invoke the @saveUrl@ function inside a compile block. For such items, the links pointing to them will be changed to the value of @url@ field from the context given to @saveUrl@ instead of the route.

It works well with [Clean URLs]("Hroch.Features.LinkResolver").

== Usage

Call @saveUrl@ with a @Context@ containing @url@ field in a @Hakyll.compile@ block (optional), then give @resolveLinksTargets@ as a monadic filter to pandoc compiler:

@
{-# LANGUAGE OverloadedStrings #-}
import Data.Maybe (fromMaybe)
import Hakyll
import Hroch.Features.LinkResolver (resolveLinksTargets, saveUrl)
import Hroch.Internal.Utils (runHakyllWithHelpFlagForTests)

pageContext :: Context String
pageContext = tweakUrl <> defaultContext

-- | Use “foo.html#body” instead of just “foo.html” in the replaced URLs.
tweakUrl :: Context a
tweakUrl = field "url" $ \item -> do
	let identifier = itemIdentifier item
	route <- fromMaybe (fail ("Trying to access url field for a page with missing route: " ++ toFilePath identifier)) <$> (getRoute identifier)
	return (toUrl route ++ "#body")

markdownCompiler :: Compiler (Item String)
markdownCompiler = pandocCompilerWithTransformM readOpts writeOpts filters
	where
		readOpts = defaultHakyllReaderOptions
		writeOpts = defaultHakyllWriterOptions
		filters = resolveLinksTargets

main :: IO ()
main = runHakyllWithHelpFlagForTests $ hakyll $ do
	match "**.md" $ do
		route $ setExtension "html"

		compile $ do
			saveUrl pageContext

			markdownCompiler
@
-}
module Hroch.Features.LinkResolver (
	resolveLinksTargets,
	saveUrl,
) where

import Control.Monad (void)
import Data.Either (fromRight)
import Data.Maybe (fromMaybe)
import Data.Text (pack, unpack)
import Hakyll (Compiler, Context, applyAsTemplate, fromFilePath, getRoute, getUnderlying, loadSnapshotBody, makeItem, saveSnapshot, toFilePath, toUrl)
import Hroch.Internal.Utils (getUriPath, isUriRelative, normalisePath)
import Hakyll.Core.Compiler.Internal (compilerTry)
import System.FilePath ((</>), takeDirectory)
import Text.Pandoc (Inline (Link), Pandoc)
import Text.Pandoc.Walk (walkM)
import qualified Text.URI as URI

routeSnapshot :: String
routeSnapshot = "__hct_route"

-- | Save nice URL of a page as a snapshot for later use in 'resolveLinksTargets'.
-- Requires that @url@ field is specified in the @ctx@.
saveUrl :: Context String -> Compiler ()
saveUrl ctx = do
	urlTemplate <- makeItem "$url$"
	url <- applyAsTemplate ctx urlTemplate
	void (saveSnapshot routeSnapshot url)
	return ()

-- | Pandoc filter that replaces relative file links with their URLs.
resolveLinksTargets :: Pandoc -> Compiler Pandoc
resolveLinksTargets doc = do
	pageIdentifier <- getUnderlying
	let pagePath = toFilePath pageIdentifier

	walkM (replaceLink pagePath) doc
	where
		replaceLink :: FilePath -> Inline -> Compiler Inline
		replaceLink pagePath (Link attrs content (target@(URI.mkURI -> Just targetUri), title)) | isUriRelative targetUri = do
			let targetPath = normalisePath (takeDirectory pagePath </> getUriPath targetUri)
			let targetIdentifier = fromFilePath targetPath

			targetRoute <- fromMaybe (error ("Page " ++ pagePath ++ " attempted to link to " ++ unpack target ++ " without a defined route.")) <$> getRoute targetIdentifier
			let routeUrl = toUrl targetRoute

			-- Try to get a clean URL from the snapshot, otherwise fall back on the route.
			urlFromSnapshot <- compilerTry (loadSnapshotBody targetIdentifier routeSnapshot)
			let newTarget = fromRight routeUrl urlFromSnapshot
			-- Re-add query string and hash fragment.
			let newTargetUrl = (fromMaybe (error ("Unable to parse new link URL " ++ newTarget ++ " when building page " ++ pagePath)) (URI.mkURI (pack newTarget))) { URI.uriQuery = URI.uriQuery targetUri, URI.uriFragment = URI.uriFragment targetUri }

			return (Link attrs content (URI.render newTargetUrl, title))
		replaceLink _pagePath i = return i
