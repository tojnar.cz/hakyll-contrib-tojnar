-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

{-|
= External metadata

This is a 'Context' that for looks up fields in @metadata.yaml@ file in the directory of the item and all its parent directories (closer wins).

== Usage

Define the field in the rules like this:

@
{-# LANGUAGE OverloadedStrings #-}
import Hakyll
import Hroch.Features.ExternalMetadata (externalMetadataField)
import Hroch.Internal.Utils (runHakyllWithHelpFlagForTests)

main :: IO ()
main = runHakyllWithHelpFlagForTests $ hakyll $ do
	match ("**.md" :: Pattern) $ do
		route $ setExtension "html"
		let ctx = externalMetadataField <> defaultContext
		compile $ pandocCompiler
			>>= loadAndApplyTemplate "templates/layout.html" ctx
@

Then, create a YAML file called @metadata.yaml@ – the dashes are currently required at the start and end of the file:

@
---
sectionTitle: Foo
---
@

This should allow you to use @$sectionTitle$@ variable in the layout.
-}
module Hroch.Features.ExternalMetadata (externalMetadataField) where

import Hakyll
import System.FilePath ((</>), splitDirectories, takeDirectory)

-- | Maps any field to its metadata value declared in external metadata.yaml files
-- in the same directory or any of its parents.
externalMetadataField :: Context a
externalMetadataField = Context $ \fieldName _ item -> do
	let
		itemDirectory = takeDirectory (toFilePath (itemIdentifier item))
		itemParents = reverse (scanl (</>) "" (splitDirectories itemDirectory))
		metadatas = map (fromFilePath . (</> "metadata.yaml")) itemParents

	loadExternalMetadata fieldName metadatas ""

loadExternalMetadata :: String -> [Identifier] -> String -> Compiler ContextField
loadExternalMetadata _fieldName [] err = noResult err
loadExternalMetadata fieldName (metaId:metas) err = do
	mvalue <- getMetadataField metaId fieldName
	case mvalue of
		Just value -> return (StringField value)
		Nothing ->
			loadExternalMetadata fieldName metas (err ++ "\nNo '" ++ fieldName ++ "' field in metadata " ++ "of item " ++ show metaId)
