-- SPDX-FileCopyrightText: 2023 Jan Tojnar
-- SPDX-License-Identifier: MIT

{-# LANGUAGE ViewPatterns #-}

{-|
= Watch server response transformer

This helper enables modifiying the bodies of HTTP responses returned by the @watch@ server on the fly. It is useful for having features that require absolute URLs of the site (e.g. RSS feeds) to work during testing. Or for injecting JavaScript files for development such as [LiveReload.js](https://github.com/livereload/livereload-js).

== Usage

@
{-# LANGUAGE OverloadedStrings #-}
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy.Search as BS
import Hakyll
import Hroch.Features.WatchTransformer (enableResponseTransformation)
import Hroch.Internal.Utils (runHakyllWithHelpFlagForTests)
import Network.Wai.Application.Static (defaultFileServerSettings)

config :: Configuration
config = defaultConfiguration {
	previewSettings = \root -> enableResponseTransformation replaceHostname (defaultFileServerSettings root)
}

replaceHostname :: ByteString -> ByteString
replaceHostname = BS.replace "https://example.org" ("http://127.0.0.1" :: ByteString)

main :: IO ()
main = runHakyllWithHelpFlagForTests $ hakyllWith config $ do
	return ()
@
-}
module Hroch.Features.WatchTransformer (
	enableResponseTransformation,
) where

import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Builder (lazyByteString, toLazyByteString)
import Data.IORef (modifyIORef', newIORef, readIORef)
import Network.HTTP.Types (Status, ResponseHeaders)
import Network.Wai (Response, responseStream, responseToStream)
import Network.Wai.Application.Static (StaticSettings, ssLookupFile)
import WaiAppStatic.Types (File, LookupResult (LRFile), Pieces, fileGetHash, fileGetModified, fileToResponse)

-- | Modifies 'StaticSettings' in such a way that the HTTP response bodies will be transformed using the provided function.
enableResponseTransformation :: (ByteString -> ByteString) -> StaticSettings -> StaticSettings
enableResponseTransformation f settings = settings {
	ssLookupFile = tweakLookupFile f settings
}

tweakLookupFile :: (ByteString -> ByteString) -> StaticSettings -> Pieces -> IO LookupResult
tweakLookupFile f settings path = do
	result <- ssLookupFile settings path
	-- Replace title and clear out caching info
	let newFile = case result of
		LRFile file -> LRFile file {
			fileToResponse = tweakResponse f file,
			fileGetHash = return Nothing,
			fileGetModified = Nothing
		}
		_ -> result
	return newFile

tweakResponse :: (ByteString -> ByteString) -> File -> Status -> ResponseHeaders -> Response
tweakResponse f file status headers =
	let
		response = fileToResponse file status headers
	in
		mapResponseBody f response

-- SPDX-SnippetCopyrightText: 2017 ryachza
-- SPDX-SnippetLicenseConcluded: CC-BY-SA-3.0
-- SPDX-SnippetLicenseComments: Based on https://stackoverflow.com/a/45485526/160386
mapResponseBody :: (ByteString -> ByteString) -> Response -> Response
mapResponseBody f response =
	let
		(status, headers, bodyConsumer) = responseToStream response
	in
		responseStream status headers $ \write flush -> do
			body <- bodyConsumer $ \streamingBody -> do
				content <- newIORef mempty
				streamingBody (\chunk -> modifyIORef' content (<> chunk)) (return ())
				readIORef content
			write (lazyByteString (f (toLazyByteString body)))
			flush

