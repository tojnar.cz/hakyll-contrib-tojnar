-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns #-}

{-|
= Thumbnailer

This is a [pandoc filter](https://pandoc.org/filters.html) that generates a thumbnail for 'Image' AST nodes marked with one of the following classes:

* @thumb-sm@
* @thumb-md@
* @thumb-lg@

The thumbnail of selected size will be placed to @public\/thumbs\/\<size\>\/@ directory. Additionally, a higher quality picture will be stored in @public\/thumbs\/hq\/@ directory.

The 'Image' node will have its source changed to the generated thumbnail and it will be wrapped inside a 'Link' node pointing to the generated HQ version. The 'Link' will have @data-original@ attribute pointing to the original URL of the source image. If the 'Image' had @data-lightbox@ attribute, it will be moved to the 'Link'.

The GD thumbnailer will fix thumbnails’ physical orientation according to the EXIF tags (when applicable) since the resizer code does not preserve the metadata.

== Usage

Create a compiler and then use it as you would 'Hakyll.Web.Pandoc.pandocCompiler'. Since the filter needs to write to file system, it is monadic and needs to be stuffed into 'Hakyll.Web.Pandoc.pandocCompilerWithTransformM'

@
{-# LANGUAGE OverloadedStrings #-}
import Hakyll
import Hroch.Features.Thumbnail (makeThumbnails)
import Hroch.Features.Thumbnail.Gd (thumbnailerGd)
import Hroch.Internal.Utils (runHakyllWithHelpFlagForTests)

markdownCompiler :: Compiler (Item String)
markdownCompiler = pandocCompilerWithTransformM readOpts writeOpts filters
	where
		readOpts = defaultHakyllReaderOptions
		writeOpts = defaultHakyllWriterOptions
		filters = makeThumbnails thumbnailerGd

main :: IO ()
main = runHakyllWithHelpFlagForTests $ hakyll $ do
	match "**.md" $ do
		compile markdownCompiler
@

You can customize the used attribute names with @makeThumbnailsWith legacyThumbnailStyle { … } thumbnailer@. Overriding 'ThumbnailStyle' fields will also allow you to change thumbnail sizes for each breakpoint.

In addition to the provided GD-based thumbnailer, you can use a custom one:

@
{-# LANGUAGE OverloadedStrings #-}
import Hakyll
import Hroch.Features.Thumbnail (ThumbnailGenerator, ThumbnailStyle, ThumbSize, makeThumbnails, sizeDims)
import Hroch.Internal.Utils (runHakyllWithHelpFlagForTests)
import System.Directory (canonicalizePath, createDirectoryIfMissing)
import System.FilePath (takeDirectory)
import System.Process (callProcess)

thumbnailer :: ThumbnailGenerator
thumbnailer style source destinations =
	mapM_ (\(size, destination) -> generateThumbnail style size source destination) destinations

generateThumbnail :: ThumbnailStyle -> ThumbSize -> FilePath -> FilePath -> IO ()
generateThumbnail style size source destination = do
	let (width, height) = sizeDims style size
	createDirectoryIfMissing True (takeDirectory destination)
	destination' <- canonicalizePath destination
	callProcess "vipsthumbnail" [source, "--size", show width ++ "x" ++ show height, "-o", destination']
	putStrLn $ "  generated thumbnail " ++ destination

markdownCompiler :: Compiler (Item String)
markdownCompiler = pandocCompilerWithTransformM readOpts writeOpts filters
	where
		readOpts = defaultHakyllReaderOptions
		writeOpts = defaultHakyllWriterOptions
		filters = makeThumbnails thumbnailer

main :: IO ()
main = runHakyllWithHelpFlagForTests $ hakyll $ do
	match "**.md" $ do
		compile markdownCompiler
@

== Current limitations

* Generating thumbnails is pretty slow and since Hakyll does not know about it, we cannot even use the new parallel execution.
* It does too many things.
* GD thumbnailer will crash when encountering an image marked as mirrored in EXIF data, not sure how common these are.
-}
module Hroch.Features.Thumbnail (
	ThumbnailGenerator,
	ThumbSize (..),
	ThumbnailStyle (..),
	makeThumbnails,
	makeThumbnailsWith,
	legacyThumbnailStyle,
	sizeDims,
) where

import Control.Monad (when)
import Data.Maybe (fromMaybe)
import Data.Text (Text, pack, unpack)
import Hakyll (Compiler, destinationDirectory, fromFilePath, getRoute, getUnderlying, toFilePath, toUrl, unsafeCompiler)
import Hroch.Internal.Utils (getUriPath, isUriRelative, normalisePath, optional)
import Hakyll.Core.Compiler.Internal (compilerAsk, compilerConfig)
import System.Directory (doesFileExist)
import System.FilePath ((</>), takeDirectory)
import Text.Pandoc (Inline (Image, Link), Pandoc)
import Text.Pandoc.Walk (walkM)
import qualified Text.URI as URI

type ThumbnailGenerator = ThumbnailStyle -> FilePath -> [(ThumbSize, FilePath)] -> IO ()

data ThumbSize = Sm | Md | Lg | Hq

sizeToDir :: ThumbSize -> FilePath
sizeToDir Sm = "sm"
sizeToDir Md = "md"
sizeToDir Lg = "lg"
sizeToDir Hq = "hq"

sizeDims :: ThumbnailStyle -> ThumbSize -> (Int, Int)
sizeDims style Sm = thumbnailStyleSmDimensions style
sizeDims style Md = thumbnailStyleMdDimensions style
sizeDims style Lg = thumbnailStyleLgDimensions style
sizeDims style Hq = thumbnailStyleHqDimensions style

data ThumbnailStyle = ThumbnailStyle {
	thumbnailStyleSmDimensions :: (Int, Int),
	thumbnailStyleMdDimensions :: (Int, Int),
	thumbnailStyleLgDimensions :: (Int, Int),
	thumbnailStyleHqDimensions :: (Int, Int),
	thumbnailStyleLightboxActivationAttr :: String,
	-- ^ Attribute name to be used on links which trigger the lightbox.
	thumbnailStyleOriginalUrlAttr :: String
	-- ^ Name of an attribute pointing to the original image.
}

-- | Thumbnail style for older Fancybox.
legacyThumbnailStyle :: ThumbnailStyle
legacyThumbnailStyle = ThumbnailStyle {
	thumbnailStyleSmDimensions = (300, 200),
	thumbnailStyleMdDimensions = (600, 400),
	thumbnailStyleLgDimensions = (900, 600),
	thumbnailStyleHqDimensions = (1200, 800),
	thumbnailStyleLightboxActivationAttr = "data-lightbox",
	thumbnailStyleOriginalUrlAttr = "data-original"
}

-- | Pandoc filter that generates thumbnails for use with lightbox.
makeThumbnails :: ThumbnailGenerator -> Pandoc -> Compiler Pandoc
makeThumbnails = makeThumbnailsWith legacyThumbnailStyle

-- | Pandoc filter that generates thumbnails for use with lightbox.
makeThumbnailsWith :: ThumbnailStyle -> ThumbnailGenerator -> Pandoc -> Compiler Pandoc
makeThumbnailsWith style thumbnailGenerator doc = do
	config <- compilerConfig <$> compilerAsk
	walkM (replaceImage (destinationDirectory config)) doc
	where
		replaceImage :: FilePath -> Inline -> Compiler Inline
		replaceImage dest img@(Image (imgId, imgClasses, imgAttrs) content (targetSrc@(URI.mkURI -> Just targetUri), targetTitle)) =
			case thumbSize imgClasses of
				Just size ->
					do
						pageIdentifier <- getUnderlying
						let pagePath = toFilePath pageIdentifier

						when (not (isUriRelative targetUri)) (fail ("Page " ++ pagePath ++ " attempted to create a thumbnail from absolute path " ++ unpack targetSrc ++ ", which is not supported."))

						let targetPath = getUriPath targetUri
						let sourcePath = takeDirectory pagePath </> targetPath
						sourceRoute <- fromMaybe (error ("Page " ++ pagePath ++ " attempted to create a thumbnail from " ++ unpack targetSrc ++ " without a defined route, which is not supported.")) <$> getRoute (fromFilePath sourcePath)

						let targetUrl = toUrl (normalisePath sourceRoute)
						let thumbPath = dest </> "thumbs" </> sizeToDir size </> sourceRoute
						let thumbUrl = toUrl (normalisePath ("thumbs" </> sizeToDir size </> sourceRoute))
						let scaledPath = dest </> "thumbs/hq" </> sourceRoute
						let scaledUrl = toUrl (normalisePath ("thumbs/hq" </> sourceRoute))

						thumbExists <- unsafeCompiler $ doesFileExist thumbPath
						scaledExists <- unsafeCompiler $ doesFileExist scaledPath
						let destinations = optional (not thumbExists) (size, thumbPath) <> optional (not scaledExists) (Hq, scaledPath)

						unsafeCompiler $ thumbnailGenerator style sourcePath destinations

						return $ Link
							(case lookup "data-lightbox" imgAttrs of
								Just gallery -> ("", [], [(pack (thumbnailStyleLightboxActivationAttr style), gallery), (pack (thumbnailStyleOriginalUrlAttr style), pack targetUrl)])
								Nothing -> ("", [], [(pack (thumbnailStyleOriginalUrlAttr style), pack targetUrl)]))
							[Image (imgId, imgClasses, filter ((/= pack (thumbnailStyleLightboxActivationAttr style)) . fst) imgAttrs) content (pack thumbUrl, targetTitle)]
							(pack scaledUrl, "")
				Nothing ->
					return img
		replaceImage _dest o = return o

-- | Determine a thumbnail size from the list of classes.
thumbSize :: [Text] -> Maybe ThumbSize
thumbSize classes
	| "thumb-sm" `elem` classes = Just Sm
	| "thumb-md" `elem` classes = Just Md
	| "thumb-lg" `elem` classes = Just Lg
	| otherwise = Nothing
