{
  description = "Opinionated static site generator";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, flake-compat, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        haskellPackages = pkgs.haskellPackages.override (attrs: {
          overrides = self.haskellOverlay;
        });

      in
      {
        devShells = {
          default = self.packages.${system}.hroch.env.overrideAttrs (attrs: {
            nativeBuildInputs = attrs.nativeBuildInputs ++ [
              pkgs.cabal-install
              pkgs.vips
            ];
          });
        };

        packages = {
          hroch = haskellPackages.hroch;

          default = self.packages.${system}.hroch;
        };
      }
    ) // {
      haskellOverlay = final: prev:
        {
          hroch = final.callPackage ./hroch.nix { };
        };
    };
}
