# Revision history for hroch

## 0.3.0 – 2024-07-14
- Cabal 3.8 or newer is now required.
- `Hroch.Internal.Utils` is now exported, API does not follow semver.
- `implicitFigures` & co. were moved from `Hakyll.Contrib.Tojnar.Gallery` to `Hroch.Features.Figures`.
- Add `hroch` static site generator program.
- The library has been renamed to `hroch`, and `Hakyll.Contrib.Tojnar` namespace changed to `Hroch.Features`.
- Add `cleanPath` and `uncleanPath` (`Hroch.Features.CleanUrls`).
- `makeThumbnailsWith` now correctly uses attribute names from `ThumbnailStyle`.
- Add `WatchTransformer`, helper for transforming the response bodies returned by the `watch` server.

## 0.2.2 – 2023-05-29
- Ensure `LinkResolver` and `Thumbnail` do not crash on Windows when root-relative link targets are used.
- Ensure `LinkResolver` and `Thumbnail` do not crash when absolute link targets are used.
- Make `LinkResolver` support links with query string or hash fragment.
- Make `LinkResolver` support relative paths from parent directories.

## 0.2.1 – 2023-01-14
- Improve Windows compatibility (ensure backslash as path separator is supported).
- Improve correctness of thumbnail path generation.
- Add `cleanRoute` and `cleanUrlField` (`Hakyll.Contrib.Tojnar.CleanUrls`) to support [clean URLs](https://en.wikipedia.org/wiki/Clean_URL).
- Add `resolveLinksTargets` and `saveUrl` (`Hakyll.Contrib.Tojnar.LinkResolver`) to allow using links pointing to unprocessed files and have Hakyll resolve them to appropriate route.

## 0.2.0 – 2023-01-01
- **BC BREAK** Changed `makeThumbnailsWith` (`Hakyll.Contrib.Tojnar.Thumbnail`) to no longer accept output path.
- **BC BREAK** Changed `makeThumbnailsWith` and `makeThumbnails` (`Hakyll.Contrib.Tojnar.Thumbnail`) to require a thumbnailer argument. You can use `gdThumbnailer` if available on your platform, or create a custom one.
- Added a `gd` Cabal flag to control presence of `Hakyll.Contrib.Tojnar.Thumbnail.Gd` module and requirement of `gd` and `hsexif` packages.

## 0.1.1 – 2021-12-02
- Loosened version bounds.
- Added `externalMetadataField` (`Hakyll.Contrib.Tojnar.ExternalMetadata`).
- Added `implicitFiguresWith` (`Hakyll.Contrib.Tojnar.Gallery`) for customizing gallery style.
- Added `makeThumbnailsWith` (`Hakyll.Contrib.Tojnar.Thumbnail`) for customizing thumbnail style.

## 0.1.0 – 2021-08-20
- Loosened version bounds.
- Updated Nix overlay.
- Added documentation.
- Added simple docs type checking.

## 0.0.5 – 2021-03-30
Fixed compatibility with latest hakyll & pandoc.

## 0.0.4 – 2020-11-03
Fixed compatibility with latest pandoc.

## 0.0.3 – 2018-01-18
Loosened version bounds, making it compatible with Nixpkgs unstable.

## 0.0.2 – 2017-06-24
Loosened version bounds, making it compatible with Stackage LTS 6.35.

## 0.0.1 – 2017-06-21
Initial commit containing gallery, thumbnail and menu helpers
