{ mkDerivation
, base
, bytestring
, containers
, directory
, doctest
, extra
, filepath
, gd
, hakyll
, hsexif
, http-types
, mtl
, modern-uri
, pandoc
, pandoc-types
, process
, regex-applicative
, stringsearch
, tasty
, tasty-hunit
, time
, unicode-show
, wai
, wai-app-static
, lib
}:

mkDerivation {
  pname = "hroch";
  version = "0.3.0";

  src = ./.;

  libraryHaskellDepends = [
    base
    bytestring
    directory
    filepath
    gd
    hakyll
    hsexif
    http-types
    mtl
    modern-uri
    pandoc
    pandoc-types
    wai
    wai-app-static
  ];

  executableHaskellDepends = [
    base
    containers
    directory
    extra
    filepath
    hakyll
    process
    time
  ];

  testHaskellDepends = [
    base
    doctest
    regex-applicative
    stringsearch
    tasty
    tasty-hunit
    unicode-show
  ];

  description = "Opinionated static site generator";
  license = lib.licenses.mit;
}
